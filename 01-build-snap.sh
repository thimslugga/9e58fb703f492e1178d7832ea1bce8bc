#!/bin/bash

# https://ubuntu.com/tutorials/create-your-first-snap#2-getting-started

snap install --classic snapcraft

lxd init --auto

mkdir -p mysnaps/mssql-server-2022 && cd mysnaps/mssql-server-2022

snapcraft init && rm -f snap/snapcraft.yaml

# download attached snapcraft.yml

snapcraft

sudo snap install --devmode *_amd64.snap