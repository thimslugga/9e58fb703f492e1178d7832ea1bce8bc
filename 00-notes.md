# Notes

## Manual Installation

```bash
sudo su -

apt-get update -y && apt-get upgrade -qqy
snap refresh

#apt-add-repository --remove universe
apt-add-repository --remove multiverse
apt-add-repository --remove restricted

pro config set apt_news=false

cat <<'EOF' | tee /etc/apt/apt.conf.d/99custom-no-install-recommends
// Disable the auto-install of recommended packages
APT::Install-Recommends "false";
// Disable the install of suggested packages
APT::Install-Suggests "false";
EOF

apt -o APT::Get::Always-Include-Phased-Updates=true install -qqy locales \
 ca-certificates \
 ssh-import-id \
 curl \
 wget \
 gnupg \
 apt-transport-https \
 software-properties-common \
 dbus \
 policykit-1 \
 systemd-container \
 libnss-mymachines \
 libnss-systemd \
 libnss3

curl https://packages.microsoft.com/keys/microsoft.asc \
  | sudo tee /etc/apt/trusted.gpg.d/microsoft.asc

curl -fsSL https://packages.microsoft.com/config/ubuntu/22.04/mssql-server-2022.list \
  | sudo tee /etc/apt/sources.list.d/mssql-server-2022.list
  
sudo apt -o APT::Get::Always-Include-Phased-Updates=true update
sudo apt -o APT::Get::Always-Include-Phased-Updates=true install -qqy mssql-server
```

With universe disabled, need libc++1 from universe:

```console
root@ip-10-10-11-59:/etc/apt/sources.list.d# apt -o APT::Get::Always-Include-Phased-Updates=true install mssql-server
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 mssql-server : Depends: libc++1 but it is not installable
E: Unable to correct problems, you have held broken packages.
```

With universe enabled, no issues:

```console
root@ip-10-10-11-59:/etc/apt/sources.list.d# apt-add-repository universe
root@ip-10-10-11-59:/etc/apt/sources.list.d# apt -o APT::Get::Always-Include-Phased-Updates=true install mssql-server
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  gdb libatomic1 libbabeltrace1 libboost-regex1.74.0 libc++1 libc++1-14 libc++abi1-14 libdebuginfod-common libdebuginfod1 libipt2 libsasl2-modules-gssapi-mit libsource-highlight-common libsource-highlight4v5 libsss-nss-idmap0 libunwind-14
Suggested packages:
  gdb-doc gdbserver clang
Recommended packages:
  libc-dbg
The following NEW packages will be installed:
  gdb libatomic1 libbabeltrace1 libboost-regex1.74.0 libc++1 libc++1-14 libc++abi1-14 libdebuginfod-common libdebuginfod1 libipt2 libsasl2-modules-gssapi-mit libsource-highlight-common libsource-highlight4v5 libsss-nss-idmap0 libunwind-14 mssql-server
0 upgraded, 16 newly installed, 0 to remove and 0 not upgraded.
Need to get 272 MB of archives.
After this operation, 1304 MB of additional disk space will be used.
Do you want to continue? [Y/n] y

...

+--------------------------------------------------------------+
Please run 'sudo /opt/mssql/bin/mssql-conf setup'
to complete the setup of Microsoft SQL Server
+--------------------------------------------------------------+
```

Files within /opt/mssql/bin directory:

```
root@ip-10-10-11-59:~# ls -lA /opt/mssql/bin/
total 4772
-rwxr-xr-x 1 root root     794 Nov 16 15:52 compress-dump.sh
-rwxr-xr-x 1 root root   21675 Nov 16 15:52 crash-support-functions.sh
-rwxr-xr-x 1 root root    1749 Nov 16 15:52 generate-sql-dump.sh
-rwxr-xr-x 1 root root    4095 Nov 16 15:52 handle-crash.sh
-rwxr-xr-x 1 root root     277 Nov 16 15:43 mssql-conf
-rwxr-xr-x 1 root root 2367768 Nov 16 15:52 paldumper
-rwxr-xr-x 1 root root 2470320 Nov 16 15:52 sqlservr
```

Removal of mssql-server and the list of packages no longer neeeded:

```console
root@ip-10-10-11-59:~# apt remove mssql-server
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following packages were automatically installed and are no longer required:
  gdb libatomic1 libbabeltrace1 libboost-regex1.74.0 libc++1 libc++1-14 libc++abi1-14 libdebuginfod-common libdebuginfod1 libipt2 libsource-highlight-common libsource-highlight4v5 libsss-nss-idmap0 libunwind-14
Use 'apt autoremove' to remove them.
The following packages will be REMOVED:
  mssql-server
0 upgraded, 0 newly installed, 1 to remove and 0 not upgraded.
After this operation, 1286 MB disk space will be freed.
Do you want to continue? [Y/n] y
(Reading database ... 95644 files and directories currently installed.)
Removing mssql-server (16.0.4105.2-2) ...
Processing triggers for man-db (2.10.2-1) ...
Processing triggers for libc-bin (2.35-0ubuntu3.6) ...
```

Building the snap with snapcraft and getting output of potential missing dependencies:

```
ubuntu@ip-10-10-11-59:~/mysnaps/mssql-server-2022$ snapcraft
Generated snap metadata
Lint warnings:
- library: opt/mssql/bin/paldumper: missing dependency 'liblber-2.5.so.0'. (provided by 'libldap-2.5-0') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/bin/paldumper: missing dependency 'libldap-2.5.so.0'. (provided by 'libldap-2.5-0') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/bin/paldumper: missing dependency 'libsasl2.so.2'. (provided by 'libsasl2-2') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/bin/paldumper: missing dependency 'libnuma.so.1'. (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/bin/paldumper: missing dependency 'libsss_nss_idmap.so.0'. (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/bin/sqlservr: missing dependency 'liblber-2.5.so.0'. (provided by 'libldap-2.5-0') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/bin/sqlservr: missing dependency 'libldap-2.5.so.0'. (provided by 'libldap-2.5-0') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/bin/sqlservr: missing dependency 'libsasl2.so.2'. (provided by 'libsasl2-2') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/bin/sqlservr: missing dependency 'libnuma.so.1'. (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/bin/sqlservr: missing dependency 'libsss_nss_idmap.so.0'. (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/lib/libc++.so.1: missing dependency 'libatomic.so.1'. (provided by 'libatomic1') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/lib/libc++.so.1.0: missing dependency 'libatomic.so.1'. (provided by 'libatomic1') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/lib/libsqlvdi.so: missing dependency 'liblber-2.5.so.0'. (provided by 'libldap-2.5-0') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/lib/libsqlvdi.so: missing dependency 'libldap-2.5.so.0'. (provided by 'libldap-2.5-0') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/lib/libsqlvdi.so: missing dependency 'libsasl2.so.2'. (provided by 'libsasl2-2') (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/lib/libsqlvdi.so: missing dependency 'libnuma.so.1'. (https://snapcraft.io/docs/linters-library)
- library: opt/mssql/lib/libsqlvdi.so: missing dependency 'libsss_nss_idmap.so.0'. (https://snapcraft.io/docs/linters-library)
- library: libc++.so.1: unused library 'opt/mssql/lib/libc++.so.1'. (https://snapcraft.io/docs/linters-library)
- library: libc++.so.1: unused library 'opt/mssql/lib/libc++.so.1.0'. (https://snapcraft.io/docs/linters-library)
- library: libc++abi.so.1: unused library 'opt/mssql/lib/libc++abi.so'. (https://snapcraft.io/docs/linters-library)
- library: libc++abi.so.1: unused library 'opt/mssql/lib/libc++abi.so.1'. (https://snapcraft.io/docs/linters-library)
- library: libc++abi.so.1: unused library 'opt/mssql/lib/libc++abi.so.1.0'. (https://snapcraft.io/docs/linters-library)
- library: libunwind-coredump.so.0: unused library 'opt/mssql/lib/libunwind-coredump.so.0.0.0'. (https://snapcraft.io/docs/linters-library)
- library: libunwind-ptrace.so.0: unused library 'opt/mssql/lib/libunwind-ptrace.so.0.0.0'. (https://snapcraft.io/docs/linters-library)
- library: libunwind-setjmp.so.0: unused library 'opt/mssql/lib/libunwind-setjmp.so.0.0.0'. (https://snapcraft.io/docs/linters-library)
```

## Related Links

- https://packages.microsoft.com/ubuntu/22.04/mssql-server-2022/pool/main/m/mssql-server/
- https://learn.microsoft.com/en-us/sql/linux/sql-server-linux-release-notes-2022?view=sql-server-ver16
- https://learn.microsoft.com/en-us/sql/linux/quickstart-install-connect-ubuntu?view=sql-server-ver16&tabs=ubuntu2204
- https://learn.microsoft.com/en-us/sql/linux/sql-server-linux-setup?view=sql-server-ver16
- https://learn.microsoft.com/en-us/sql/linux/sql-server-linux-configure-mssql-conf?view=sql-server-ver16
- https://learn.microsoft.com/en-us/azure/azure-sql/virtual-machines/windows/performance-guidelines-best-practices-checklist?view=azuresql
- https://ubuntu.com/blog/microsoft-sql-server-on-ubuntu
- https://discourse.ubuntu.com/t/microsoft-sql-server-2019-on-ubuntu-20-04/21943